package main

import "fmt"

func potencia(x, y int) int {
	pot := 1
	for z := y; z > 0; z-- {
		pot *= x
	}
	return pot
}

func main() {
	x := 0
	y := 0

	fmt.Println("Insira a base da potencia")
	fmt.Scan(&x)

	fmt.Println("Insira a portencia")
	fmt.Scan(&y)

	resultado := potencia(x, y)

	fmt.Println(x, "elevado a", y, "é", resultado)
}
