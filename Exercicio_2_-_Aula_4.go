package main

import "fmt"

func conversao(x, y float32) (float32, float32) {
	return ((x * 1.8) + 32), ((y - 32) / 1.8)
}

func main() {
	var z int
	var x, y float32

	fmt.Println("Escolha a conversão que deseja")
	fmt.Println("1. Celsius para Fahrenheit")
	fmt.Println("2. Fahrenheit para Celsius")
	fmt.Scan(&z)

	switch z {
	case 1:
		fmt.Println("Digite a temperatura em Celcius")
		fmt.Scan(&x)
		tempfah, _ := conversao(x, y)
		fmt.Println(x, "em Fahrenheit é", tempfah)
	case 2:
		fmt.Println("Digite a temperatura em Fahrenheit")
		fmt.Scan(&y)
		_, tempcelcius := conversao(x, y)
		fmt.Println(y, "em Celcius é", tempcelcius)
	}
}
