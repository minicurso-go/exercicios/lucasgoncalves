
package main
import "fmt"

func media (soma float64, y float64) float64 {
    return soma / y
}

func main() {
  x := 0.0
  y := 0.0
  soma:= 0.0
  
  fmt.Println("Insira numeros: ")
  fmt.Println("(Insira 0 para encerrar a entrada)")
  
  for {
      fmt.Scan(&x)
      if x == 0{
          break
      }
      soma += x  
      y++
  }
  
  resultado := media(soma,y)
  fmt.Println("A media dos numeros inseridos é", resultado)
  fmt.Println("A soma dos numeros inseridos é", soma)
  fmt.Println("a quantidade de termos inseridos é", y)
}