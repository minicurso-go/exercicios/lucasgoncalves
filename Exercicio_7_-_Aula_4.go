package main

import "fmt"

func primo(x int) {
	resultado := 1
	for y := x; y > 0; y-- {
		resultado = x % y
		if resultado == 0 && y != x && y != 1 {
			fmt.Println(x, "não é primo")
			return
		}
	}
	fmt.Println(x, "é primo")
}

func main() {
	x := 0
	fmt.Println("Insira um numero para saber se ele é primo")
	fmt.Scan(&x)
	primo(x)
}
