package main

import "fmt"

func soma(x, y float32) float32 {
	return x + y
}

func main() {
	var x, y float32

	fmt.Println("Insira um valor x")
	fmt.Scan(&x)

	fmt.Println("Insira um valor y")
	fmt.Scan(&y)

	resultado := soma(x, y)

	fmt.Println("O resultado da soma entre a e b é", resultado)
}
