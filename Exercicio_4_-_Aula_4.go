package main

import "fmt"

func fatorial(x int) int {
	resultado := 1
	for y := x; y > 0; y-- {
		resultado *= y
	}
	return resultado
}

func main() {
	x := 0

	fmt.Println("Insira o numero que voce deseja saber o fatorial:")
	fmt.Scan(&x)

	resultado := fatorial(x)

	fmt.Println(x, "fatorial é", resultado)
}
